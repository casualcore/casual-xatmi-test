import os
import casual.make.plumbingimpl.directive as plumbing

from casual.middleware.make.dsl import (
    IncludePaths, LibraryPaths, Compile, LinkArchive, LinkServer, LinkExecutable, Install
)

INCLUDE_PATHS = [
    'include',
    '$(CASUAL_HOME)/include',
    '$(CASUAL_BUILD_HOME)/middleware/serviceframework/include',
    '$(CASUAL_BUILD_HOME)/middleware/common/include',
    '$(CASUAL_BUILD_HOME)/thirdparty/rapidjson/include'
]

LIBRARY_PATHS = [
    'bin',
    '$(CASUAL_BUILD_HOME)/middleware/serviceframework/bin',
    '$(CASUAL_BUILD_HOME)/middleware/common/bin',
    '$(CASUAL_BUILD_HOME)/middleware/configuration/bin',
    '$(CASUAL_HOME)/lib'
]

if 'WITH_TUXEDO' in os.environ:
    INCLUDE_PATHS.append('$(TUXDIR)/include')
    LIBRARY_PATHS.append('$(TUXDIR)/lib')

if 'WITH_DB2' in os.environ:
    INCLUDE_PATHS.append('$(DB2DIR)/include')
    LIBRARY_PATHS.append('$(DB2DIR)/lib64')

IncludePaths(INCLUDE_PATHS)

LibraryPaths(LIBRARY_PATHS)

if 'WITH_DB2' in os.environ:
    DB2_PRECOMPILE_COMMAND = '/build/build-tools/internal/db2precompile'

    def db2_compile(sqc_file, bind_file, package_name):

        sqc_file = plumbing.normalize_path(sqc_file)

        temp_source_file = sqc_file + '.temp'

        source_file = sqc_file + '.cpp'

        plumbing.add_rule(
            source_file,
            prerequisites=[sqc_file],
            recipes=[
                (DB2_PRECOMPILE_COMMAND + ' PREP tst001 tst001 ' +
                 sqc_file + ' ' + bind_file + ' ' + temp_source_file + ' ' + package_name),
                'mv ' + temp_source_file + ' ' + source_file
            ]
        )

        return Compile(source_file, directive=' -Wno-narrowing')


COMMON_ARCHIVE = LinkArchive('bin/common-archive', [
    Compile('source/serialize.cpp'),
    Compile('source/task.cpp')
])

INSTALL_BIN = []

TARGET = LinkServer('bin/casual-xatmi-test-server', [
    Compile('source/server.cpp')
], [
    COMMON_ARCHIVE,
    'casual-xatmi',
    'casual-common',
    'casual-sf',
], 'source/server.yaml')

INSTALL_BIN.append(TARGET)

TARGET = LinkExecutable('bin/casual-xatmi-test-client', [
    Compile('source/client.cpp')
], [
    COMMON_ARCHIVE,
    'casual-xatmi',
    'casual-common',
    'casual-sf',
])

INSTALL_BIN.append(TARGET)

if 'WITH_DB2' in os.environ:
    BIND_FILE = 'bin/db2server.bnd'

    TARGET = LinkServer('bin/casual-xatmi-test-resource-db2', [
        db2_compile('source/resource/db2server.sqc', BIND_FILE, 'db2server')
    ], [
        COMMON_ARCHIVE,
        'casual-xatmi',
        'casual-common',
        'casual-sf',
        'db2',
    ], 'source/resource/db2server.server.yaml', resources=['TMSUDB2'])

    INSTALL_BIN.append(TARGET)
    INSTALL_BIN.append(BIND_FILE)

Install(INSTALL_BIN, '$(CASUAL_XATMI_TEST_HOME)/bin')
