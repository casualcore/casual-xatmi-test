//!
//! casual
//!

#include "model.h"
#include "serialize.h"
#include "task.h"

#include <sf/archive/json.h>
#include <common/algorithm.h>
#include <common/log/category.h>
#include <common/functional.h>

#include <xatmi.h>


extern "C"
{
   void xatmi_configure( TPSVCINFO* information);
   void task( TPSVCINFO* information);

   int tpsvrinit( int argc, char **argv)
   {
      return tpunadvertise( "task");
   }

}


namespace casual
{
   namespace xatmi
   {

      namespace test
      {

         namespace server
         {
            namespace local
            {
               namespace
               {
                  namespace task
                  {
                     auto entry( TPSVCINFO* information)
                     {
                        std::vector< model::task::Task> tasks;
                        test::serialize::json( information->data, information->len, CASUAL_MAKE_NVP( tasks));

                        auto results = test::task::execute( std::move( tasks));
                        return test::serialize::json( CASUAL_MAKE_NVP( results));
                     }
                  } // task

                  namespace configuration
                  {

                     auto setup( TPSVCINFO* information)
                     {
                        model::configuration::Setup setup;
                        test::serialize::json( information->data, information->len, CASUAL_MAKE_NVP( setup));

                        tpunadvertise( "xatmi_configure");

                        model::configuration::Reply result;
                        result.pid = common::process::id().value();

                        common::algorithm::transform( setup.services, result.services, []( auto& s){
                           tpadvertise( s.name.c_str(), &::task);
                           return s;
                        });

                        return test::serialize::json( CASUAL_MAKE_NVP( result));
                     }
                  } // configuration



                  namespace service
                  {

                     template< typename C>
                     void invocation( TPSVCINFO* information, C&& caller)
                     {

                        struct
                        {
                           int outcome = TPSUCCESS;
                           long code = 0;
                           char* data = nullptr;
                           long len = 0;

                        } xatmi_result;

                        //
                        // Scope to ensure we don't leave anything on the stack with a dtor.
                        //
                        {
                           auto buffer = common::invoke( caller, information);

                           //
                           // use the same buffer that we got from the call
                           //
                           xatmi_result.data = tprealloc( information->data, buffer.size());
                           xatmi_result.len = tptypes( xatmi_result.data, nullptr, nullptr);

                           //
                           // Copy payload
                           //
                           std::copy(
                              std::begin( buffer),
                              std::begin( buffer) + ( xatmi_result.len < static_cast< long>( buffer.size()) ? xatmi_result.len : static_cast< long>( buffer.size())),
                              xatmi_result.data);

                        }

                        tpreturn(
                           xatmi_result.outcome,
                           xatmi_result.code,
                           xatmi_result.data,
                           xatmi_result.len,
                           0);

                     }

                  } // service
               } // <unnamed>
            } // local

            void configure( TPSVCINFO* information)
            {
               current::information( information);
               local::service::invocation( information, &local::configuration::setup);

            }

            namespace task
            {
               void entry( TPSVCINFO* information)
               {
                  current::information( information);
                  local::service::invocation( information, &local::task::entry);
               }
            } // task
         } // server
      } // test
   } // xatmi
} // casual


extern "C"
{
   void xatmi_configure( TPSVCINFO* information)
   {
      casual::xatmi::test::server::configure( information);
   }

   void task( TPSVCINFO* information)
   {
      casual::xatmi::test::server::task::entry( information);
   }
}

