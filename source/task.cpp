//!
//! casual
//!

#include "task.h"

#include "serialize.h"

#include <common/log.h>

#include <xatmi.h>
#include <tx.h>

#include <algorithm>

namespace casual
{
   namespace xatmi
   {
      namespace test
      {

         namespace local
         {
            namespace
            {
               struct Task
               {
                  template< typename T>
                  Task( T&& task) : m_holder( std::make_unique< Holder< T>>( std::move( task))) {}

                  void prepare() { m_holder->prepare();}
                  model::task::Result result()   { return m_holder->result();}

               private:
                  struct Base
                  {
                     virtual void prepare() = 0;
                     virtual model::task::Result result() = 0;
                  };

                  template< typename I>
                  struct Holder : Base
                  {
                     Holder( I implementation) : m_implementation( std::move( implementation)) {}

                     void prepare() override { m_implementation.prepare();}
                     model::task::Result result() override  { return m_implementation.result();}

                  private:
                     I m_implementation;
                  };

                  std::unique_ptr< Base> m_holder;
               };


               Task transform( model::task::Task& task);

               namespace implementation
               {
                  struct task_holder
                  {
                     task_holder( model::task::Task task) : task( std::move( task)) {}

                  protected:
                     model::task::Task task;
                     model::clock_type::time_point m_start;
                  };

                  struct tpacall : task_holder
                  {
                     using task_holder::task_holder;

                     void prepare()
                     {
                        auto buffer = serialize::buffer::json( sf::name::value::pair::make( "tasks", task.tasks));

                        m_start = model::clock_type::now();

                        m_descriptor = ::tpacall( task.value.c_str(), buffer.data.get(), buffer.len, 0);
                     }

                     model::task::Result result()
                     {
                        model::task::Result result( task);

                        if( m_descriptor > 0)
                        {
                           auto buffer = test::buffer::allocate( 256);
                           auto data = buffer.data.release();

                           auto status = tpgetrply( &m_descriptor, &data, &buffer.len, 0);
                           buffer.data.reset( data);

                           if( status != -1)
                           {
                              serialize::json(buffer.data.get(), buffer.len, sf::name::value::pair::make( "results", result.results));
                           }
                           else
                           {
                              result.information = tperrnostring( tperrno);
                           }
                        }
                        else
                        {
                           result.information = tperrnostring( tperrno);
                        }
                        result.metric = model::task::Metric( m_start, model::clock_type::now());
                        return result;
                     }
                  private:
                     int m_descriptor = 0;

                  };


                  struct tpcall : task_holder
                  {
                     using task_holder::task_holder;

                     void prepare()
                     {
                     }

                     model::task::Result result()
                     {
                        model::task::Result result( task);
                        auto start = model::clock_type::now();


                        auto output = test::buffer::allocate( 256);

                        {
                           auto input = serialize::buffer::json( sf::name::value::pair::make( "tasks", task.tasks));

                           auto data = output.data.release();

                           if( ::tpcall( task.value.c_str(), input.data.get(), input.len, &data, &output.len, 0) == -1)
                           {
                              result.information = tperrnostring( tperrno);
                           }
                           else
                           {
                              output.data.reset( data);
                              serialize::json( output.data.get(), output.len, sf::name::value::pair::make( "results", result.results));
                           }
                        }
                        result.metric = model::task::Metric( start, model::clock_type::now());
                        return result;
                     }
                  };

                  namespace arbitary
                  {

                     struct tpacall : task_holder
                     {
                        using task_holder::task_holder;

                        void prepare()
                        {
                           auto input = serialize::buffer::payload( task.payload, X_OCTET, nullptr);

                           m_start = model::clock_type::now();

                           m_descriptor = ::tpacall( task.value.c_str(), input.data.get(), input.len, 0);
                        }

                        model::task::Result result()
                        {
                           model::task::Result result( task);

                           if( m_descriptor > 0)
                           {
                              auto buffer = test::buffer::allocate( 256);
                              auto data = buffer.data.release();

                              auto status = tpgetrply( &m_descriptor, &data, &buffer.len, 0);
                              buffer.data.reset( data);

                              if( status != -1)
                              {
                                 if( buffer.data)
                                    result.payload.assign( buffer.data.get(), buffer.len);
                              }
                              else
                              {
                                 result.information = tperrnostring( tperrno);
                              }
                           }
                           else
                           {
                              result.information = tperrnostring( tperrno);
                           }

                           result.metric = model::task::Metric( m_start, model::clock_type::now());
                           return result;
                        }
                     private:
                        int m_descriptor = 0;
                     };


                     struct tpcall : task_holder
                     {
                        using task_holder::task_holder;

                        void prepare()
                        {
                        }

                        model::task::Result result()
                        {
                           model::task::Result result( task);
                           auto start = model::clock_type::now();

                           {
                              auto output = test::buffer::allocate( 256);
                              auto input = serialize::buffer::payload( task.payload, X_OCTET, nullptr);

                              auto data = output.data.release();

                              if( ::tpcall( task.value.c_str(), input.data.get(), input.len, &data, &output.len, 0) == -1)
                              {
                                 result.information = tperrnostring( tperrno);
                                 return result;
                              }

                              output.data.reset( data);

                              if( output.data)
                                 result.payload.assign( output.data.get(), output.len);
                           }

                           result.metric = model::task::Metric( start, model::clock_type::now());
                           return result;
                        }
                     };


                  } // arbitary

                  namespace transaction
                  {
                     struct Base
                     {
                        Base( model::task::Task& task)
                         : m_result( task)
                        {
                        }
                     protected:
                        model::task::Result m_result;
                     };

                     struct Begin : Base
                     {
                        using Base::Base;
                        void prepare()
                        {
                           common::Trace trace{ "xatmi::test::local::implemenation::transaction::Begin::prepare"};
                           auto start = model::clock_type::now();
                           m_result.information = std::to_string( tx_begin());
                           m_result.metric = model::task::Metric( start, model::clock_type::now());
                        }
                        model::task::Result result()
                        {
                           common::Trace trace{ "xatmi::test::local::implemenation::transaction::Begin::result"};
                           return m_result;
                        }
                     };

                     struct Commit : Base
                     {
                        using Base::Base;

                        void prepare()
                        {
                           common::Trace trace{ "xatmi::test::local::implemenation::transaction::Commit::prepare"};
                        }
                        model::task::Result result()
                        {
                           common::Trace trace{ "xatmi::test::local::implemenation::transaction::Commit::result"};
                           auto start = model::clock_type::now();

                           auto tx = tx_commit();

                           if( tx != TX_OK)
                           {
                              m_result.information = std::to_string( tx);
                              tx_rollback();
                           }

                           m_result.metric = model::task::Metric( start, model::clock_type::now());
                           return m_result;
                        }
                     };

                     struct Rollback : Base
                     {
                        using Base::Base;

                        void prepare()
                        {
                           common::Trace trace{ "xatmi::test::local::implemenation::transaction::Rollback::prepare"};
                        }
                        model::task::Result result()
                        {
                           common::Trace trace{ "xatmi::test::local::implemenation::transaction::Rollback::result"};
                           auto start = model::clock_type::now();
                           m_result.information = std::to_string( tx_rollback());
                           m_result.metric = model::task::Metric( start, model::clock_type::now());
                           return m_result;
                        }
                     };
                  } // transaction

                  struct Iterate : task_holder
                  {
                     using task_holder::task_holder;

                     void prepare() {}
                     model::task::Result result()
                     {
                        common::Trace trace{ "xatmi::test::local::implemenation::Iterate::result"};

                        model::task::Result result( task);
                        auto start = model::clock_type::now();

                        common::log::debug << "task: " << task << '\n';

                        auto count = std::stol( result.value);

                        while( count-- > 0)
                        {
                           result.aggregate( task::execute( task.tasks));
                        }
                        common::log::debug << "result: " << result << '\n';

                        result.metric = model::task::Metric( start, model::clock_type::now());

                        return result;
                     }
                  };

                  struct Sequential
                  {
                     Sequential( model::task::Task&& task)
                     {
                        m_result = task;

                        std::transform(
                           std::begin( task.tasks), std::end( task.tasks),
                           std::back_inserter( m_executers),
                           &local::transform);

                     }

                     void prepare()
                     {
                        common::Trace trace{ "xatmi::test::local::implemenation::transaction::Sequential::prepare"};

                        auto start = model::clock_type::now();
                        for( auto& task : m_executers)
                        {
                           task.prepare();
                           m_result.results.push_back( task.result());
                        }
                        m_result.metric = model::task::Metric( start, model::clock_type::now());
                     }

                     model::task::Result result()
                     {
                        common::Trace trace{ "xatmi::test::local::implemenation::transaction::Sequential::result"};
                        return m_result;
                     }

                  private:
                     std::vector< local::Task> m_executers;
                     model::task::Result m_result;
                  };

               } // implementation

               Task transform( model::task::Task& task)
               {
                  static std::map< std::string, std::function< local::Task( model::task::Task)>> mapping{
                     {
                        model::task::type::call_service_async,
                        []( model::task::Task task){ return local::implementation::tpacall{ std::move( task)};}
                     },
                     {
                        model::task::type::call_service,
                        []( model::task::Task task){ return local::implementation::tpcall{ std::move( task)};}
                     },
                     {
                        model::task::type::transaction_begin,
                        []( model::task::Task task){ return local::implementation::transaction::Begin{ task};}
                     },
                     {
                        model::task::type::transaction_commit,
                        []( model::task::Task task){ return local::implementation::transaction::Commit{ task};}
                     },
                     {
                        model::task::type::transaction_rollback,
                        []( model::task::Task task){ return local::implementation::transaction::Rollback{ task};}
                     },
                     {
                        model::task::type::call_arbitary_service,
                        []( model::task::Task task){ return local::implementation::arbitary::tpcall{  std::move( task)};}
                     },
                     {
                        model::task::type::call_arbitary_service_async,
                        []( model::task::Task task){ return local::implementation::arbitary::tpacall{  std::move( task)};}
                     },
                     {
                        model::task::type::iterate,
                        []( model::task::Task task){ return local::implementation::Iterate{ std::move( task)};}
                     },
                     {
                        model::task::type::sequential,
                        []( model::task::Task task){ return local::implementation::Sequential{ std::move( task)};}
                     },
                  };

                  auto found = mapping.find( task.type);

                  if( found != std::end( mapping))
                  {
                     return found->second( std::move( task));
                  }

                  throw std::runtime_error{ "failed to find task type: " + task.type};
               }

            } // <unnamed>
         } // local


         namespace task
         {
            std::vector< model::task::Result> execute( std::vector< model::task::Task> tasks)
            {
               common::Trace trace{ "xatmi::test::task::execute"};

               common::log::debug << "tasks: " << common::range::make( tasks) << '\n';

               //
               // Transform all tasks to 'dispatchers'
               //
               std::vector< local::Task> executers;

               std::transform(
                  std::begin( tasks), std::end( tasks),
                  std::back_inserter( executers),
                  &local::transform);

               // prepare
               std::for_each( std::begin( executers), std::end( executers), std::mem_fn( &local::Task::prepare));

               std::vector< model::task::Result> results;

               // collect result
               std::transform(
                  std::begin( executers), std::end( executers),
                  std::back_inserter( results),
                  std::mem_fn( &local::Task::result));

               common::log::debug << "results: " << common::range::make( results) << '\n';

               return results;
            }

         } // task


      } // test

   } // xatmi


} // casual

