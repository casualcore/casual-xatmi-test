//!
//! casual
//!

#include "model.h"
#include "serialize.h"
#include "task.h"

#include <common/exception/handle.h>
#include <common/argument.h>
#include <sf/archive/maker.h>
#include <sf/archive/log.h>


#include <iostream>

namespace casual
{
   namespace xatmi
   {
      namespace test
      {
         namespace local
         {
            namespace
            {
               template< typename NVP>
               buffer::Raw call( const std::string& service, NVP&& nvp)
               {
                  auto input = serialize::buffer::json( std::forward< NVP>( nvp), "X_OCTET", nullptr);
                  auto output = buffer::allocate( "X_OCTET", nullptr, 128);
                  auto raw = output.data.release();

                  tpcall( service.c_str(), input.data.get(), input.len, &raw, &output.len, 0);
                  output.data.reset( raw);

                  return output;
               }


               model::configuration::Reply setup( const model::configuration::Setup& setup)
               {
                  auto buffer = call( "xatmi_configure", CASUAL_MAKE_NVP( setup));

                  model::configuration::Reply result;
                  serialize::json( buffer.data.get(), buffer.len, CASUAL_MAKE_NVP( result));

                  return result;
               }


               struct Settings
               {
                  struct
                  {
                     std::string input = "yaml";
                     std::string output = "yaml";
                  } format;

               };



               void main(int argc, char **argv)
               {
                  Settings settings;

                  {
                     auto format_completer = []( auto, bool){
                        return std::vector< std::string>{ "json", "xml", "yaml", "ini"};
                     };
                     common::argument::Parse parse( "starts a test case provided on stdin, and output the result on stdout",
                        common::argument::Option( std::tie( settings.format.input), format_completer, { "--format-input"}, "input format\ndefault 'yaml'"),
                        common::argument::Option( std::tie( settings.format.output), format_completer, { "--format-output"}, "output format\ndefault 'yaml'")
                     );

                     parse( argc, argv);
                  }

                  auto reader = sf::archive::reader::from::name( std::cin, settings.format.input);
                  model::objective::Request objective;

                  reader >> CASUAL_MAKE_NVP( objective);

                  model::objective::Reply result;

                  std::transform( std::begin( objective.setup), std::end( objective.setup),
                     std::back_inserter( result.setup), &setup);

                  result.result = task::execute( std::move( objective.tasks));

                  auto archive = sf::archive::writer::from::name( std::cout, settings.format.output);
                  archive << CASUAL_MAKE_NVP( result);

               }
            } // <unnamed>
         } // local

      } // test
   } // xatmi
} // casual





int main(int argc, char **argv)
{
   try
   {
      casual::xatmi::test::local::main( argc, argv);
      return 0;
   }
   catch( ...)
   {
      return casual::common::exception::handle(std::cerr);
   }

}



