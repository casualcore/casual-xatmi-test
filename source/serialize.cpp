//!
//! casual
//!


#include "serialize.h"

namespace casual
{
   namespace xatmi
   {
      namespace test
      {

         namespace current
         {
            namespace local
            {
               namespace
               {
                  const TPSVCINFO* information = nullptr;
               } // <unnamed>
            } // local
            const TPSVCINFO* information() { return local::information;}
            void information( const TPSVCINFO* info)
            {
               local::information = info;
               tptypes( info->data, buffer::type().name.data(), buffer::type().subname.data());
            }


            namespace buffer
            {
               Type& type()
               {
                  static Type singleton{ { "X_OCTET"}, {""}};
                  return singleton;
               }

            } // buffer
         } // current

         namespace buffer
         {
            Raw allocate( const char* type, const char* subtype, long size)
            {
               Raw result;

               result.data.reset( tpalloc( type, subtype, size));
               result.len = result.data ? size : 0L;

               return result;
            }
            Raw allocate( long size)
            {
               return allocate( current::buffer::type().name.data(), current::buffer::type().subname.data(), size);
            }

         } // buffer

      } // test
   } // xatmi
} // casual
