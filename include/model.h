//!
//! casual
//!

#ifndef SERVER_INCLUDE_TASK_MODEL_H_
#define SERVER_INCLUDE_TASK_MODEL_H_


#include <sf/namevaluepair.h>
#include <common/algorithm.h>

#include <vector>
#include <string>
#include <chrono>

namespace casual
{
   namespace xatmi
   {
      namespace test
      {
         namespace model
         {
            using clock_type = std::chrono::steady_clock;
            using time_resolution = std::chrono::nanoseconds;

            namespace task
            {
               namespace type
               {
                  constexpr auto call_service = "call-service";

                  constexpr auto call_service_async = "call-service-async";
                  constexpr auto enqueue = "enqueue";
                  constexpr auto dequeue = "dequeue";
                  constexpr auto transaction_begin = "transaction-begin";
                  constexpr auto transaction_commit = "transaction-commit";
                  constexpr auto transaction_rollback = "transaction-rollback";
                  constexpr auto call_arbitary_service = "call-arbitary-service";
                  constexpr auto call_arbitary_service_async = "call-arbitary-service-async";

                  constexpr auto iterate = "iterate";
                  constexpr auto sequential = "sequential";
               } // type

               struct Common
               {
                  std::string type;
                  std::string description;
                  std::string value;

                  CASUAL_CONST_CORRECT_SERIALIZE(
                     archive & CASUAL_MAKE_NVP( type);
                     archive & CASUAL_MAKE_NVP( description);
                     archive & CASUAL_MAKE_NVP( value);
                  )

               };

               struct Metric
               {
                  Metric() = default;

                  Metric( clock_type::time_point start, clock_type::time_point end)
                   : count( 1), total( std::chrono::duration_cast< time_resolution>( end - start)),
                     min( total), max( total)
                  {
                  }


                  std::size_t count = 0;
                  time_resolution total{};
                  time_resolution min{};
                  time_resolution max{};

                  friend Metric& operator += ( Metric& lhs, const Metric& rhs)
                  {
                     if( rhs.count)
                     {
                        lhs.count += rhs.count;
                        lhs.total += rhs.total;
                        lhs.min = std::min( lhs.min, rhs.min);
                        lhs.max = std::max( lhs.max, rhs.max);
                     }

                     return lhs;
                  }

                  inline friend std::ostream& operator << ( std::ostream& out, const Metric& value)
                  {
                     return out << "{ count: " << value.count
                        << ", total: " << value.total.count()
                        << ", min: " << value.min.count()
                        << ", max: " << value.max.count()
                        << '}';
                  }

                  CASUAL_CONST_CORRECT_SERIALIZE(
                     archive & CASUAL_MAKE_NVP( count);
                     archive & CASUAL_MAKE_NVP( total);
                     archive & CASUAL_MAKE_NVP( min);
                     archive & CASUAL_MAKE_NVP( max);
                  )
               };


               struct Task : Common
               {
                  std::string payload;

                  std::vector< Task> tasks;

                  CASUAL_CONST_CORRECT_SERIALIZE(
                     Common::serialize( archive);
                     archive & CASUAL_MAKE_NVP( payload);
                     archive & CASUAL_MAKE_NVP( tasks);
                  )

                  inline friend std::ostream& operator << ( std::ostream& out, const Task& value)
                  {
                     return out << "{ type: " << value.type
                        << ", description: " << value.description
                        << ", value: " << value.value
                        << ", payload: " << value.payload
                        << ", tasks: " << common::range::make( value.tasks)
                        << '}';
                  }
               };


               struct Result : Common
               {

                  Result() = default;
                  Result( const Task& rhs) : Common( rhs)
                  {

                  }

                  inline Result& operator = ( const Task& rhs)
                  {
                     static_cast< Common&>( *this) = rhs;
                     return *this;
                  }

                  std::string information;
                  std::string payload;
                  Metric metric;

                  inline void aggregate( const std::vector< Result>& value)
                  {
                     auto target = common::range::make( results);
                     results.reserve( value.size());

                     for( auto& result : value)
                     {
                        if( target)
                        {
                           (*target) += result;
                           ++target;
                        }
                        else
                        {
                           results.push_back( result);
                        }
                     }
                  }


                  friend Result& operator += ( Result& lhs, const Result& rhs)
                  {
                     lhs.metric += rhs.metric;

                     lhs.aggregate( rhs.results);

                     return lhs;
                  }



                  std::vector< Result> results;

                  CASUAL_CONST_CORRECT_SERIALIZE(
                     Common::serialize( archive);
                     archive & CASUAL_MAKE_NVP( payload);
                     archive & CASUAL_MAKE_NVP( metric);
                     archive & CASUAL_MAKE_NVP( information);
                     archive & CASUAL_MAKE_NVP( results);
                  )

                  inline friend std::ostream& operator << ( std::ostream& out, const Result& value)
                  {
                     return out << "{ type: " << value.type
                        << ", description: " << value.description
                        << ", value: " << value.value
                        << ", payload: " << value.payload
                        << ", metric: " << value.metric
                        << ", information: " << value.information
                        << ", results: " << common::range::make( value.results)
                        << '}';
                  }
               };

            } // task

            namespace configuration
            {
               struct Service
               {
                  std::string name;

                  CASUAL_CONST_CORRECT_SERIALIZE(
                     archive & CASUAL_MAKE_NVP( name);
                  )

               };

               struct Setup
               {
                  std::vector< Service> services;

                  CASUAL_CONST_CORRECT_SERIALIZE(
                     archive & CASUAL_MAKE_NVP( services);
                  )
               };

               struct Reply
               {
                  int pid;
                  std::vector< Service> services;

                  CASUAL_CONST_CORRECT_SERIALIZE(
                     archive & CASUAL_MAKE_NVP( pid);
                     archive & CASUAL_MAKE_NVP( services);
                  )
               };

            } // configuration

            namespace objective
            {
               struct Request
               {
                  std::vector< configuration::Setup> setup;
                  std::vector< task::Task> tasks;

                  CASUAL_CONST_CORRECT_SERIALIZE(
                     archive & CASUAL_MAKE_NVP( setup);
                     archive & CASUAL_MAKE_NVP( tasks);
                  )
               };

               struct Reply
               {
                  std::vector< configuration::Reply> setup;
                  std::vector< task::Result> result;

                  CASUAL_CONST_CORRECT_SERIALIZE(
                     archive & CASUAL_MAKE_NVP( setup);
                     archive & CASUAL_MAKE_NVP( result);
                  )
               };

            } // objective
         } // model
      } // test
   } // xatmi
} // casual



#endif /* SERVER_INCLUDE_TASK_MODEL_H_ */
