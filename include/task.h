/*
 * task.h
 *
 *  Created on: 22 juni 2017
 *      Author: 40043280
 */

#ifndef INCLUDE_TASK_H_
#define INCLUDE_TASK_H_

#include "model.h"

#include <memory>

namespace casual
{
   namespace xatmi
   {
      namespace test
      {

         namespace task
         {
            std::vector< model::task::Result> execute( std::vector< model::task::Task> tasks);

         } // task


      } // test

   } // xatmi


} // casual



#endif /* INCLUDE_TASK_H_ */
