//!
//! casual
//!

#ifndef INCLUDE_SERIALIZE_H_
#define INCLUDE_SERIALIZE_H_


#include <sf/archive/json.h>

#include <xatmi.h>


#include <array>
#include <memory>

namespace casual
{
   namespace xatmi
   {
      namespace test
      {

         namespace current
         {
            const TPSVCINFO* information();
            void information( const TPSVCINFO* info);

            namespace buffer
            {
               struct Type
               {
                  std::array< char, 9> name;
                  std::array< char, 17> subname;
               };

               Type& type();

            } // buffer
         } // current


         namespace buffer
         {
            struct Raw
            {
               struct Deleter
               {
                  void operator() ( char* buffer) const { tpfree( buffer);}
               };
               std::unique_ptr< char, Deleter> data;
               long len = 0;
            };

            Raw allocate( const char* type, const char* subtype, long size);
            Raw allocate( long size);

         } // buffer

         namespace serialize
         {

            template< typename NVP>
            std::vector< char> json( NVP&& namevaluepair)
            {
               std::vector< char> buffer;
               
               auto archive = sf::archive::json::writer( buffer);
               archive << namevaluepair;

               return buffer;
            }

            template< typename NVP>
            void json( const std::vector< char>& buffer, NVP&& namevaluepair)
            {
               auto archive = sf::archive::json::reader( buffer);
               archive >> namevaluepair;
            }

            template< typename NVP>
            void json( char* data, long len, NVP&& namevaluepair)
            {
               // TODO: add "range" to reader api so we don't need a vector char
               std::vector< char> buffer( 0, len);
               std::copy( data, data + len, std::begin( buffer));

               json( buffer, std::forward< NVP>( namevaluepair));
            }



            namespace buffer
            {
               template< typename P>
               test::buffer::Raw payload( const P& payload, const char* type, const char* subtype)
               {
                  test::buffer::Raw result;

                  result.data.reset( tpalloc( type, subtype, payload.size()));
                  result.len = result.data ? payload.size() : 0L;

                  std::copy(
                     std::begin( payload),
                     std::begin( payload) + ( result.len < static_cast< long>( payload.size()) ? result.len : static_cast< long>( payload.size())),
                     result.data.get());

                  return result;
               }

               template< typename NVP>
               test::buffer::Raw json( NVP&& namevaluepair, const char* type, const char* subtype)
               {
                  auto buffer = serialize::json( std::forward< NVP>( namevaluepair));

                  return buffer::payload( buffer, type, subtype);
               }

               template< typename NVP>
               test::buffer::Raw json( NVP&& namevaluepair)
               {
                  return json( std::forward< NVP>( namevaluepair), current::buffer::type().name.data(), current::buffer::type().subname.data());
               }


            } // buffer

         } // serialize

      } // test
   } // xatmi
} // casual



#endif /* INCLUDE_SERIALIZE_H_ */
