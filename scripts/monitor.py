#!/usr/bin/env python

import sys
import os
import os.path
import threading
import subprocess
import argparse
import Queue
import json
import getpass

from datetime import datetime


class AsynchronousFileReader(threading.Thread):

    def __init__(self, fd, queue):
        assert callable(fd.readline)
        assert isinstance(queue, Queue.Queue)

        threading.Thread.__init__(self)

        self._fd = fd
        self._queue = queue

    def run(self):
        for line in iter(self._fd.readline, ''):
            self._queue.put(line.strip())

    def eof(self):
        return not self.is_alive() and self._queue.empty()


class Monitor(object):

    def __init__(self):
        self._threads = []
        self._args = self._parse_arguments()
        self._result = {}
        self._timestamp = datetime.now().strftime('%Y%m%d%H%M%S')
        self._date = datetime.now().strftime('%m%d%y')
        self._name = '{testcase}-{system}-{timestamp}'.format(
            testcase=self._args.testcase,
            system=self._args.system,
            timestamp=self._timestamp
        )

        self._args.result_dir = os.path.join(self._args.result_dir, self._name)

        assert not os.path.exists(self._args.result_dir), (
            '{dir} already exists'.format(self._args.result_dir)
        )

        os.makedirs(self._args.result_dir)

    def _parse_arguments(self):
        def _valid_directory(value):
            if os.path.isdir(value) and os.access(value, os.W_OK):
                return value
            else:
                raise argparse.ArgumentTypeError(
                    '{dir} does not exists, or is not writeable'.format(
                        dir=value
                    )
                )

        def _valid_system(value):
            systems = ['casual', 'other']

            if value.lower() in systems:
                return value.lower()
            else:
                raise argparse.ArgumentTypeError(
                    '-s/--system must be one of {system}'.format(
                        system=', '.join(systems)
                    )
                )

        parser = argparse.ArgumentParser(
            description='Monitors CPU, memory etc.'
        )

        parser.add_argument(
            '--hosts', nargs='+', type=str, required=True,
            help='Hosts that should be monitored'
        )

        parser.add_argument(
            '-i', '--interval', type=int, required=False, default=1,
            help='Interval (seconds) of collecting monitoring information'
        )

        parser.add_argument(
            '-d', '--duration', type=int, required=False, default=1,
            help='Duration (seconds) that the hosts should be monitored'
        )

        parser.add_argument(
            '--users', nargs='+', type=str, required=False, default=None,
            help=(
                'Usernames for the specified hosts for which the domains are'
                ' running as'
            )
        )

        parser.add_argument(
            '--result-dir', type=_valid_directory, required=False,
            default=os.path.join(
                os.path.dirname(os.path.abspath(__file__)), 'results'
            ),
            help='Base directory to store results'
        )

        parser.add_argument(
            '-t', '--testcase', type=str, required=True,
            help='Testcase that is executing'
        )

        parser.add_argument(
            '-s', '--system', type=_valid_system, required=False,
            default='casual',
            help='Type of the system to be monitored'
        )

        args = parser.parse_args()

        if args.users is not None and len(args.users) != len(args.hosts):
            parser.error(
                'the number of specified --users most match the number of '
                'specified --hosts'
            )

        return args

    def _wait(self):
        if len(self._threads) < 1:
            return

        try:
            for thread in self._threads:
                thread.join()
        except KeyboardInterrupt:
            for thread in self._threads:
                thread.stop()

        del self._threads[:]

    def _monitor_host(
        self, host, commands, output=None, output_format='json',
        user=None, with_duration=True
    ):

        if host not in self._result:
            self._result[host] = []

        command = [
            'ssh', '-o', 'StrictHostKeyChecking=no',
            '-o', 'UserKnownHostsFile=/dev/null',
            '-o', 'LogLevel=ERROR', host,
        ]

        if user and not user == getpass.getuser():
            command.extend(
                ['\"sudo su - {user} --session-command \''.format(user=user)]
            )
            if with_duration:
                command.extend(['timeout {duration}'.format(
                    duration=self._args.duration
                )])
            command.extend(
                ['bash -l -s\'\" <<< \"', ' && '.join(commands), '\"']
            )
        else:
            if with_duration:
                command.extend(['timeout {duration}'.format(
                    duration=self._args.duration
                )])

            command.extend([
                'bash -l -s', '<<EOF\n',
                '\n'.join(commands), '\nEOF'
            ])

        command = ' '.join(command)

        process = subprocess.Popen(
            command, bufsize=-1, stdout=subprocess.PIPE, shell=True
        )

        if output:
            queue = Queue.Queue()
            reader = AsynchronousFileReader(process.stdout, queue)
            reader.start()

            result = []

            while not reader.eof():
                while not queue.empty():
                    result.append(queue.get())

            with open(
                os.path.join(
                    self._args.result_dir,
                    '{host}-{output}.{output_format}'.format(
                        host=host, output=output,
                        output_format=output_format
                    )
                ), 'w'
            ) as fd:
                if output_format == 'json':
                    json.dump(result, fd, indent=2)
                else:
                    fd.write('\n'.join(result))

    def monitor_hosts(self):
        # ugghhhh...
        command = (
            'while true; do ps wwo pid,pcpu,rss,vsize,size,cmd '
            '--no-headers | egrep -v -- \\\"no-headers\\\" | '
            'gawk \'{{print \\\"{{'
            '\\\\\\"time\\\\\\": \\\" strftime(\\\"%s\\\") \\\", '
            '\\\\\\"pid\\\\\\": \\\" \$1 \\\", '
            '\\\\\\"cpu\\\\\\": \\\\\\"\\\" \$2 \\\"\\\\\\", '
            '\\\\\\"rss\\\\\\": \\\" \$3 \\\", '
            '\\\\\\"vsize\\\\\\": \\\" \$4 \\\", '
            '\\\\\\"size\\\\\\": \\\" \$5 \\\", '
            '\\\\\\"command\\\\\\": \\\\\\"\\\" \$6 \\\"\\\\\\"'
            '}}\\\"}}\'; sleep {interval}; done'.format(
                interval=self._args.interval
            )
        )

        self._broadcast_command(command, 'utilization', 'json', True)

    def _broadcast_command(
        self, command, output=None, output_format='json', with_duration=False
    ):
        local_user = getpass.getuser()

        for index, host in enumerate(self._args.hosts):
            if self._args.users is not None:
                user = self._args.users[index]
            else:
                user = local_user

            thread = threading.Thread(
                target=self._monitor_host, args=(
                    host, [command], output, output_format, user, with_duration
                )
            )

            thread.start()
            self._threads.append(thread)

        self._wait()

    def monitor_pre(self):
        if self._args.system == 'casual':
            command = (
                'cp \$CASUAL_DOMAIN_HOME/casual.log '
                '\$CASUAL_DOMAIN_HOME/casual.log.{timestamp} && '
                '> \$CASUAL_DOMAIN_HOME/casual.log && '
                'casual service --metric-reset'
            ).format(timestamp=self._timestamp)

            commands = [
                [
                    'casual service --state json',
                    'casual-service-before',
                    'json'
                ],
                [
                    'casual service --list-services --no-color',
                    'casual-service-before',
                    'log'
                ]
            ]
        else:
            command = (
                'cp \$HOME/ULOG.{date} \$HOME/ULOG.{date}.{timestamp} && '
                '> \$HOME/ULOG.{date} && '
                'cp \$HOME/txrpt.stat \$HOME/txrpt.stat.{timestamp} && '
                '> \$HOME/txrpt.stat'.format(
                    timestamp=self._timestamp,
                    date=self._date
                )
            )
            commands = []

        self._broadcast_command(command)

        for command in commands:
            self._broadcast_command(*command)

    def monitor_post(self):
        if self._args.system == 'casual':
            command = 'cat \$CASUAL_DOMAIN_HOME/casual.log'
            commands = [
                [
                    'casual service --state json',
                    'casual-service-after',
                    'json'
                ],
                [
                    'casual service --list-services --no-color',
                    'casual-service-after',
                    'log'
                ]
            ]
        else:
            command = 'cat \$HOME/ULOG.{date}'.format(date=self._date)
            commands = [
                [
                    'cat \$HOME/txrpt.stat',
                    'txrpt',
                    'stat'
                ]
            ]

        self._broadcast_command(
            command, self._args.system, 'log'
        )

        for command in commands:
            self._broadcast_command(*command)

    def main(self):
        self.monitor_pre()
        self.monitor_hosts()
        self.monitor_post()

        print 'Results are stored in {dir}'.format(dir=self._args.result_dir)

        return 0


if __name__ == '__main__':
    try:
        sys.exit(Monitor().main())
    except (argparse.ArgumentError, Exception) as e:
        print e
