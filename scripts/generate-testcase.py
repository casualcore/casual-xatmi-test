#!/usr/bin/env python

import sys
import argparse
import os.path

from jinja2 import Template


def valid_testcase(value):
    try:
        if '.yaml' in value and '.tpl.' not in value:
            value = '{0}.tpl.yaml'.format(os.path.splitext(value)[0])
        elif '.tpl.yaml' not in value:
            value = '{0}.tpl.yaml'.format(value)

        with open(value, 'r'):
            return value

    except IOError as e:
        raise argparse.ArgumentTypeError(e)


def valid_type(value):
    calls = [
        'call-service', 'call-service-async', 'call-arbitary-service',
        'call-arbitary-service-async'
    ]

    if value in calls:
        return value
    else:
        raise argparse.ArgumentTypeError(
            '{0} is not a valid type, should be one of {1}'.format(
                value, ', '.join(calls)
            )
        )


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='generate a testcase based on a template'
    )

    parser.add_argument(
        '-t', '--testcase', dest='template', required=True, type=valid_testcase,
        help='testcase to generate from template'
    )

    parser.add_argument(
        '--type', required=False, type=valid_type,
        help='type of call'
    )

    parser.add_argument(
        '-i', '--iterations', required=True, type=int,
        help='number of iterations'
    )

    parser.add_argument(
        '-f', '--flows', required=True, type=int,
        help='number of parallell flows'
    )

    parser.add_argument(
        '-s', '--service', required=False, type=str,
        help='service to call during load test'
    )

    return parser.parse_args()


def main():
    args = parse_arguments()

    values = {
        'iterations': args.iterations,
        'service': args.service,
        'type': args.type,
        'flows': args.flows
    }

    with open(args.template, 'r') as fd:
        template = Template(fd.read())

    print(template.render(**values))

    return 0


if __name__ == '__main__':
    sys.exit(main())
