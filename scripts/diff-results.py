#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import sys
import json
import argparse

PY3 = sys.version_info[0] == 3

if PY3:
    STRING_TYPES = str,
else:
    STRING_TYPES = basestring,

DECIMAL_POINTS = 4

COLUMN_HEADERS = [
    'description', 'level', 'type', 'metric', 'left', 'right', 'delta', 'ratio'
]
ROW_FORMAT = (
    '| {0:{description}} | {1: >{level}} | {2:{type}} | {3:{metric}} | '
    '{4: >{left}} | {5: >{right}} | {6: >{delta}} | {7: >{ratio}} |'
)
BORDER_FORMAT = (
    '+-{0}-+-{1}-+-{2}-+-{3}-+-{4}-+-{5}-+-{6}-+-{7}-+'
)
COLUMN_WIDTH = {}
for label in COLUMN_HEADERS:
    COLUMN_WIDTH[label] = len(label)


def valid_input(value):
    try:
        with open(value, 'r') as fd:
            json.loads(fd.read())
            return value
    except (IOError, json.decoder.JSONDecodeError) as e:
        raise argparse.ArgumentTypeError(e)


def valid_flow(value):
    try:
        return 'flow {number} '.format(
            number=int(value)
        )
    except:
        argparse.ArgumentTypeError(
            '--flows value must be an integer, {value} is not valid'.format(
                value=value
            )
        )


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='compare two results'
    )

    parser.add_argument(
        'logfiles', nargs=2, metavar='FILE', type=valid_input,
        help='log files to compare'
    )

    parser.add_argument(
        '--flows', nargs='*', type=valid_flow,
        help='only show specified flows'
    )

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        '-d', '--dump', action='store_true',
        default=False, help='dump result json to stdout'
    )
    group.add_argument(
        '-s', '--summary', action='store_true',
        default=True, help='print summary to stdout'
    )

    return parser.parse_args()


def compare(left, right):
    result = {}

    for key in left:
        assert key in right, '{0} is not in right side'.format(key)
        assert isinstance(left[key], type(right[key])), (
            '{0}, left = {1}, right = {2}'
            .format(key, type(left[key]), type(right[key]))
        )

        if isinstance(left[key], dict):
            result[key] = compare(left[key], right[key])
        elif isinstance(left[key], list):
            result[key] = []
            assert len(left[key]) == len(right[key]), (
                '{0}: lists are unbalanced, left = {1}, right = {2}'
                .format(key, len(left[key]), len(right[key]))
            )
            for index, _ in enumerate(left[key]):
                result[key].append(compare(left[key][index], right[key][index]))
        elif isinstance(left[key], STRING_TYPES):
            assert left[key] == right[key], (
                '{0}: different string value, left = {1}, right = {2}'
                .format(key, left[key], right[key])
            )
            result[key] = left[key]
        elif isinstance(left[key], int):
            result[key] = {
                'left': left[key],
                'right': right[key],
            }

    return result


def print_summary(results, args):
    summary(results, args, False)
    summary(results, args, True)
    print 'left, right, delta = microseconds'


def print_border():
    print(
        BORDER_FORMAT.format(
            '-' * COLUMN_WIDTH['description'],
            '-' * COLUMN_WIDTH['level'],
            '-' * COLUMN_WIDTH['type'],
            '-' * COLUMN_WIDTH['metric'],
            '-' * COLUMN_WIDTH['left'],
            '-' * COLUMN_WIDTH['right'],
            '-' * COLUMN_WIDTH['delta'],
            '-' * COLUMN_WIDTH['ratio']
        )
    )


def summary(results, args, output, level=1):
    if level == 1 and output:
        print_border()
        print(
            ROW_FORMAT.format(
                *COLUMN_HEADERS, description=COLUMN_WIDTH['description'],
                level=COLUMN_WIDTH['level'],
                type=COLUMN_WIDTH['type'], metric=COLUMN_WIDTH['metric'],
                left=COLUMN_WIDTH['left'], right=COLUMN_WIDTH['right'],
                delta=COLUMN_WIDTH['delta'], ratio=COLUMN_WIDTH['ratio']
            )
        )
        print_border()

    for result in results:
        show_border = False

        for metric, values in result['metric'].iteritems():
            if not metric == 'count':
                values['left'] = round(values['left'] / 1e3, DECIMAL_POINTS)
                values['right'] = round(values['right'] / 1e3, DECIMAL_POINTS)

            values['left'] = (
                values['left'] / result['metric']['count']['left']
            )
            values['right'] = (
                values['right'] / result['metric']['count']['right']
            )
            values['delta'] = round(
                values['left'] - values['right'], DECIMAL_POINTS
            )
            values['ratio'] = round(
                values['left'] / values['right'], DECIMAL_POINTS
            )

            if not output:
                result['description'] = '{description} '.format(
                    description=result['description'].strip()
                )
                COLUMN_WIDTH['description'] = max(
                    len(result['description']), COLUMN_WIDTH['description']
                )
                COLUMN_WIDTH['level'] = max(
                    len(str(level)), COLUMN_WIDTH['level']
                )
                COLUMN_WIDTH['type'] = max(
                    len(result['type']), COLUMN_WIDTH['type']
                )
                COLUMN_WIDTH['metric'] = max(
                    len(metric), COLUMN_WIDTH['metric']
                )
                COLUMN_WIDTH['left'] = max(
                    len(str(values['left'])), COLUMN_WIDTH['left']
                )
                COLUMN_WIDTH['right'] = max(
                    len(str(values['right'])), COLUMN_WIDTH['right']
                )
                COLUMN_WIDTH['delta'] = max(
                    len(str(values['delta'])), COLUMN_WIDTH['delta']
                )
                COLUMN_WIDTH['ratio'] = max(
                    len(str(values['ratio'])), COLUMN_WIDTH['ratio']
                )
            else:
                if metric == 'count':
                    continue

                if (
                    args.flows is None or
                    result['description'].startswith(tuple(args.flows))
                ):
                    show_border = True
                    print(
                        ROW_FORMAT.format(
                            result['description'], level, result['type'],
                            metric,
                            values['left'],
                            values['right'],
                            values['delta'],
                            values['ratio'],
                            description=COLUMN_WIDTH['description'],
                            level=COLUMN_WIDTH['level'],
                            type=COLUMN_WIDTH['type'],
                            metric=COLUMN_WIDTH['metric'],
                            left=COLUMN_WIDTH['left'],
                            right=COLUMN_WIDTH['right'],
                            delta=COLUMN_WIDTH['delta'],
                            ratio=COLUMN_WIDTH['ratio']
                        )
                    )

        if show_border:
            print_border()

        if 'results' in result:
            summary(result['results'], args, output, level+1)


def main():
    args = parse_arguments()

    file_left, file_right = args.logfiles

    with open(file_left, 'r') as fd:
        json_left = json.loads(fd.read())

    with open(file_right, 'r') as fd:
        json_right = json.loads(fd.read())

    result = compare(json_left, json_right)
    if args.dump:
        print(json.dumps(result, indent=2, sort_keys=True))
    elif args.summary:
        print_summary(result['result']['result'], args)
    else:
        return 1

    return 0


if __name__ == '__main__':
    sys.exit(main())
