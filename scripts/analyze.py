#!/usr/bin/env python

import sys
import argparse
import json
import __builtin__

ROW_FORMAT = (
    '| {type:{ltype}} | {service:{lservice}} | {min: >{lmin}} | '
    '{flowF:{lflowF}} | {max: >{lmax}} | {flowS:{lflowS}} | '
    '{med: >{lmed}} | {avg: >{lavg}} | {count: >{lcount}} |'
)

BORDER_FORMAT = (
    '+-{type}-+-{service}-+-{min}-+-{flowF}-+-{max}-+-{flowS}-+-{med}'
    '-+-{avg}-+-{count}-+'
)

COLUMN_HEADERS = [
    'type', 'service', 'min', 'flowF', 'max', 'flowS', 'avg', 'med', 'count'
]


def valid_json_file(value):
    try:
        with open(value, 'r') as fd:
            json.loads(fd.read())
        return value
    except Exception as e:
        raise argparse.ArgumentTypeError(e)


def parse_arguments():
    parser = argparse.ArgumentParser(
        description='analyze test results'
    )

    parser.add_argument(
        '-r', '--result', type=valid_json_file, required=True,
        help='results file in json format'
    )

    return parser.parse_args()


def print_border(width, sep='-'):
    print BORDER_FORMAT.format(
        type=sep * width['ltype'],
        service=sep * width['lservice'],
        min=sep * width['lmin'],
        flowF=sep * width['lflowF'],
        max=sep * width['lmax'],
        flowS=sep * width['lflowS'],
        med=sep * width['lmed'],
        avg=sep * width['lavg'],
        count=sep * width['lcount']
    )


def compare(method, summary, value):
    f = getattr(__builtin__, method)

    summary['metric'][method]['value'] = f(
        value['metric'][method], summary['metric'][method]['value']
    )

    if summary['metric'][method]['value'] == value['metric'][method]:
        if 'flow' in value['description']:
            summary['metric'][method]['name'] = (
                value['description'].split(' ')[1]
            )
        else:
            summary['metric'][method]['name'] = value['description']


def parse(parts, summary):
    for part in parts:
        t = part['type']
        v = part['value']
        if t not in summary:
            summary[t] = {}

        if v not in summary[t]:
            summary[t][v] = {
                'metric': {
                    'total': [],
                    'min': {
                        'name': None,
                        'value': sys.maxint
                    },
                    'max': {
                        'name': None,
                        'value': -sys.maxint-1
                    },
                    'count': part['metric']['count']
                }
            }

        summary[t][v]['metric']['total'].append(
            part['metric']['total'] / part['metric']['count'] / 1e6
        )

        for method in ['min', 'max']:
            compare(method, summary[t][v], part)

        if 'results' in part and len(part['results']) > 0:
            parse(part['results'], summary)


def median(input):
    n = len(input)
    if n < 1:
        return None

    if n % 2 == 1:
        return round(sorted(input)[n//2], 2)
    else:
        return round(sum(sorted(input)[n//2-1:n//2+1])/2.0, 2)


def average(input):
    return round(float(sum(input)) / max(len(input), 1), 2)


def main():
    args = parse_arguments()

    with open(args.result, 'r') as fd:
        input = json.loads(fd.read())

    summary = {}

    parse(input['result']['result'], summary)

    max_length = {}
    for header in COLUMN_HEADERS:
        max_length['l' + header] = len(header)

    for call_type, services in summary.iteritems():
        if call_type in ['iterate', 'sequential']:
            continue

        max_length['ltype'] = max(max_length['ltype'], len(call_type))

        for service, struct in services.iteritems():
            struct['metric']['min']['value'] /= 1e6
            struct['metric']['max']['value'] /= 1e6
            struct['metric']['avg'] = average(struct['metric']['total'])
            struct['metric']['med'] = median(struct['metric']['total'])
            del struct['metric']['total']

            max_length['ltype'] = max(len(call_type), max_length['ltype'])
            max_length['lservice'] = max(len(service), max_length['lservice'])
            max_length['lmin'] = max(
                len(str(struct['metric']['min']['value'])), max_length['lmin']
            )
            max_length['lflowF'] = max(
                len(struct['metric']['min']['name']), max_length['lflowF']
            )
            max_length['lmax'] = max(
                len(str(struct['metric']['max']['value'])), max_length['lmax']
            )
            max_length['lflowS'] = max(
                len(struct['metric']['max']['name']), max_length['lflowS']
            )
            max_length['lavg'] = max(
                len(str(struct['metric']['avg'])), max_length['lavg']
            )
            max_length['lmed'] = max(
                len(str(struct['metric']['med'])), max_length['lmed']
            )
            max_length['lcount'] = max(
                len(str(struct['metric']['count'])), max_length['lcount']
            )

    row = dict(
        map(
            lambda key: (key, key), COLUMN_HEADERS
        )
    )
    row.update(max_length)

    print_border(max_length)
    print ROW_FORMAT.format(**row)
    print_border(max_length)

    for call_type, services in summary.iteritems():
        if call_type in ['iterate', 'sequential']:
            continue

        for service, struct in services.iteritems():
            row = {
                'type': call_type,
                'service': service,
                'min': struct['metric']['min']['value'],
                'flowF': struct['metric']['min']['name'],
                'max': struct['metric']['max']['value'],
                'flowS': struct['metric']['max']['name'],
                'avg': struct['metric']['avg'],
                'med': struct['metric']['med'],
                'count': struct['metric']['count']
            }
            row.update(max_length)

            print ROW_FORMAT.format(**row)

    print_border(max_length)
    print 'min, max, med, avg = milliseconds'
    print 'flowF = fastest flow, flowS = slowest flow'

    return 0

if __name__ == '__main__':
    sys.exit(main())
