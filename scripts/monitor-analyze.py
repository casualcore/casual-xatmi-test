#!/usr/bin/env python

import sys
import argparse
import json
import datetime
import os.path

from itertools import chain

MAP_VALUES = {
    'cpu': float,
    'rss': int,
    'vsize': int,
    'size': int
}


def valid_metric(value):
    if value in MAP_VALUES:
        return value
    else:
        raise argparse.ArgumentTypeError(
            '{value} must be one of {metrics}'.format(
                value=value,
                metrics=', '.join(MAP_VALUES.keys())
            )
        )


def parse_arguments():
    parser = argparse.ArgumentParser('Create utilization report')
    parser.add_argument(
        '-r', '--results', type=str, required=True,
        help='utilization results in json'
    )

    parser.add_argument(
        '-m', '--metric', type=valid_metric, required=True,
        help='metric to show'
    )

    parser.add_argument(
        '-f', '--filter', nargs='*', type=str, help='filter on command'
    )

    return parser.parse_args()


def avg(input):
    return round(float(sum(input)) / max(len(input), 1), 2)


def print_border(widths):
    print (
        '+-{lcommand}-+-{lprocesses}-+-{lvalue}-+-{lvalue}-+-{lvalue}-+'.format(
            lcommand='-' * widths['lcommand'],
            lprocesses='-' * widths['lprocesses'],
            lvalue='-' * widths['lvalue']
        )
    )


def main():
    args = parse_arguments()
    results = None

    with open(args.results, 'r') as stream:
        results = json.load(stream)

    max_length = {
        'lcommand': len('command'),
        'lprocesses': len('processes'),
        'lvalue': len('min')
    }

    summary = {}
    for result in results:
        result = json.loads(result)
        pid = result['pid']
        command = os.path.basename(result['command'])
        if command not in summary:
            summary[command] = {
                'pid': [],
                'metrics': {}
            }

        if pid not in summary[command]['pid']:
            summary[command]['pid'].append(pid)

        max_length['lcommand'] = max(
            len(command), max_length['lcommand']
        )

        for value in MAP_VALUES:
            summary[command]['metrics'][value] = []

        result['time'] = datetime.datetime.fromtimestamp(int(result['time']))

        for value, func in MAP_VALUES.iteritems():
            summary[command]['metrics'][value].append({
                result['time']: func(result[value])
            })

            max_length['lvalue'] = max(
                len(str(result[value])) + 2, max_length['lvalue']
            )

    row = {
        'command': 'command',
        'processes': 'processes',
        'min': 'min',
        'max': 'max',
        'avg': 'avg'
    }
    row.update(max_length)

    print '{metric}'.format(metric=args.metric.upper())

    print_border(max_length)
    print (
        '| {command:{lcommand}} | {processes:{lprocesses}} | {min: >{lvalue}} '
        '| {max: >{lvalue}} | {avg: >{lvalue}} |'
    ).format(**row)
    print_border(max_length)

    for command, struct in summary.iteritems():
        if (
            args.filter is not None and not any(
                filter in command for filter in args.filter
            )
        ):
            continue

        metrics = struct['metrics'][args.metric]
        values = list(
            chain(
                *(metric.values() for metric in metrics)
            )
        )

        row = {
            'command': command,
            'processes': len(struct['pid']),
            'min': min(values),
            'max': max(values),
            'avg': avg(values)
        }
        row.update(max_length)

        print (
            '| {command:{lcommand}} | {processes:{lprocesses}} | '
            '{min:{lvalue}} | {max:{lvalue}} | {avg:{lvalue}} |'
        ).format(**row)

    print_border(max_length)

    return 0

if __name__ == '__main__':
    sys.exit(main())
