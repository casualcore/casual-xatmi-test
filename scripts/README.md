# Scripts

This directory contains a number of helper scripts to ease testing functionality and
performance with the `casual-xatmi-test` client and server.

Below are a description and usage examples for each of them.

## `analyze.py`

Help:
```
usage: analyze.py [-h] -r RESULT

analyze test results

optional arguments:
  -h, --help            show this help message and exit
  -r RESULT, --result RESULT
                        results file in json format
```

This script analyzes the `JSON` result from a `casual-xatmi-test-client --format-output json` output.
The result is a summary of each service that has been called (min, max, avg, med values).

Example:
```
$ ./analyze.py -r results/testfall-1-250-100-other-20171129104638/testfall-1-250-100-other.json
+-----------------------+-------------------------------+-----------+-------+-------------+-------+---------+---------+-------+
| type                  | service                       |       min | flowF |         max | flowS |     med |     avg | count |
+-----------------------+-------------------------------+-----------+-------+-------------+-------+---------+---------+-------+
| call-arbitary-service | xatmi_test_resouce_db2_select |  1.419197 | 27    |  513.240694 | 1     |    4.29 |    4.28 |   100 |
| call-arbitary-service | xatmi_test_resouce_db2_delete |  1.224382 | 250   |   72.401293 | 183   |    6.62 |    6.69 |   100 |
| call-arbitary-service | xatmi_test_resouce_db2_insert |  4.036752 | 135   |  639.089108 | 88    |    9.53 |    9.63 |   100 |
| call-service-async    | test_service_1                | 10.722084 | 1     | 3551.958061 | 250   | 1378.63 | 1375.11 |   100 |
+-----------------------+-------------------------------+-----------+-------+-------------+-------+---------+---------+-------+
min, max, med, avg = milliseconds
flowF = fastest flow, flowS = slowest flow
```

The result is from a test scenario that calls `test_service_1`, which in turn calls the `xatmi_test_resouce_db2_*` services. This is done 250 (flows) * 100 (iterations).

## `diff-results.py`

Help:
```
usage: diff-results.py [-h] (-d | -s) FILE FILE

compare two results

positional arguments:
  FILE           log files to compare

optional arguments:
  -h, --help     show this help message and exit
  -d, --dump     dump result json to stdout
  -s, --summary  print summary to stdout
```

This script compares the `JSON` result from `casual-xatmi-test-client --format-output json` output, between two different runs of the same test scenario.

Example:
```
$ ./diff-results.py -s results/testfall-1-250-100-casual-20171129104638/testfall-1-250-100-casual.json results/testfall-1-250-100-other-20171129110450/testfall-1-250-100-other.json
+-----------------------+-------+-----------------------+--------+---------------+---------------+-------------+--------+
| description           | level | type                  | metric |          left |         right |       delta |  ratio |
+-----------------------+-------+-----------------------+--------+---------------+---------------+-------------+--------+
| iterate 100 times     |     1 | iterate               | max    |   273171.3311 |   269045.4374 |   4125.8937 | 1.0153 |
| iterate 100 times     |     1 | iterate               | total  |   273171.3311 |   269045.4374 |   4125.8937 | 1.0153 |
| iterate 100 times     |     1 | iterate               | min    |   273171.3311 |   269045.4374 |   4125.8937 | 1.0153 |
+-----------------------+-------+-----------------------+--------+---------------+---------------+-------------+--------+
| flow 1                |     2 | call-service-async    | max    |      568.8176 |      151.8817 |    416.9359 | 3.7451 |
| flow 1                |     2 | call-service-async    | total  |     2814.1179 |     2422.9951 |    391.1228 | 1.1614 |
| flow 1                |     2 | call-service-async    | min    |       10.7221 |       12.4205 |     -1.6984 | 0.8633 |
+-----------------------+-------+-----------------------+--------+---------------+---------------+-------------+--------+
| flow 1 - db2 select   |     3 | call-arbitary-service | max    |      513.2407 |      104.3775 |    408.8632 | 4.9172 |
| flow 1 - db2 select   |     3 | call-arbitary-service | total  |      919.7995 |      467.3287 |    452.4708 | 1.9682 |
| flow 1 - db2 select   |     3 | call-arbitary-service | min    |         1.605 |         1.511 |       0.094 | 1.0622 |
+-----------------------+-------+-----------------------+--------+---------------+---------------+-------------+--------+
| flow 1 - db2 insert   |     3 | call-arbitary-service | max    |         25.32 |       28.9812 |     -3.6612 | 0.8737 |
| flow 1 - db2 insert   |     3 | call-arbitary-service | total  |      859.9452 |      935.9447 |    -75.9995 | 0.9188 |
| flow 1 - db2 insert   |     3 | call-arbitary-service | min    |        4.7704 |        5.0893 |     -0.3189 | 0.9373 |
+-----------------------+-------+-----------------------+--------+---------------+---------------+-------------+--------+
| flow 1 - db2 delete   |     3 | call-arbitary-service | max    |       21.9833 |       31.5007 |     -9.5174 | 0.6979 |
| flow 1 - db2 delete   |     3 | call-arbitary-service | total  |      766.1727 |      792.4199 |    -26.2472 | 0.9669 |
| flow 1 - db2 delete   |     3 | call-arbitary-service | min    |        2.3053 |        2.7538 |     -0.4485 | 0.8371 |
+-----------------------+-------+-----------------------+--------+---------------+---------------+-------------+--------+
.                       .       .                       .        .               .               .             .        .
.                       .       .                       .        .               .               .             .        .
.                       .       .                       .        .               .               .             .        .
+-----------------------+-------+-----------------------+--------+---------------+---------------+-------------+--------+
| flow 250              |     2 | call-service-async    | max    |     3551.9581 |     3072.4351 |     479.523 | 1.1561 |
| flow 250              |     2 | call-service-async    | total  |   272170.6175 |   268038.5272 |   4132.0903 | 1.0154 |
| flow 250              |     2 | call-service-async    | min    |     2190.7508 |     2221.4638 |     -30.713 | 0.9862 |
+-----------------------+-------+-----------------------+--------+---------------+---------------+-------------+--------+
| flow 250 - db2 select |     3 | call-arbitary-service | max    |        7.9785 |       17.8901 |     -9.9116 |  0.446 |
| flow 250 - db2 select |     3 | call-arbitary-service | total  |      427.5251 |      467.8282 |    -40.3031 | 0.9139 |
| flow 250 - db2 select |     3 | call-arbitary-service | min    |        1.7251 |        1.4971 |       0.228 | 1.1523 |
+-----------------------+-------+-----------------------+--------+---------------+---------------+-------------+--------+
| flow 250 - db2 insert |     3 | call-arbitary-service | max    |       41.6904 |       17.7776 |     23.9128 | 2.3451 |
| flow 250 - db2 insert |     3 | call-arbitary-service | total  |      980.8522 |      922.7739 |     58.0783 | 1.0629 |
| flow 250 - db2 insert |     3 | call-arbitary-service | min    |        4.7824 |        5.1561 |     -0.3737 | 0.9275 |
+-----------------------+-------+-----------------------+--------+---------------+---------------+-------------+--------+
| flow 250 - db2 delete |     3 | call-arbitary-service | max    |       20.0225 |       11.2474 |      8.7751 | 1.7802 |
| flow 250 - db2 delete |     3 | call-arbitary-service | total  |      436.3944 |       419.049 |     17.3454 | 1.0414 |
| flow 250 - db2 delete |     3 | call-arbitary-service | min    |        1.2244 |        1.2004 |       0.024 |   1.02 |
+-----------------------+-------+-----------------------+--------+---------------+---------------+-------------+--------+
left, right, delta = microseconds
```

## `generate-testcase.py`

Help:
```
usage: generate-testcase.py [-h] -t TEMPLATE [--type TYPE] -i ITERATIONS -f
                            FLOWS [-s SERVICE]

generate a testcase based on a template

optional arguments:
  -h, --help            show this help message and exit
  -t TEMPLATE, --testcase TEMPLATE
                        testcase to generate from template
  --type TYPE           type of call
  -i ITERATIONS, --iterations ITERATIONS
                        number of iterations
  -f FLOWS, --flows FLOWS
                        number of parallell flows
  -s SERVICE, --service SERVICE
                        service to call during load test
```

This script generates test scenarios based on `jinja2` templates. Predefined templates can be found in `./templates`.

Example:
```
$ ./generate-testcase.py -t templates/testcase.tpl.yaml -i 1 -f 1 --type call-arbitary-service
objective:
  tasks:
    - type: iterate
      value: 1
      description: iterate 1 times
      tasks:
        - type: call-service-async
          value: test_service_1
          description: flow 1
          tasks:
            - type: call-arbitary-service
              value: xatmi_test_resouce_db2_select
              description: flow 1 - db2 select
            - type: call-arbitary-service
              value: xatmi_test_resouce_db2_insert
              description: flow 1 - db2 insert
            - type: call-arbitary-service
              value: xatmi_test_resouce_db2_delete
              description: flow 1 - db2 delete

$ ./generate-testcase.py -t templates/testconfiguration.tpl.yaml -f 2 -i 0
objective:
    setup:
        - services:
            - name: test_service_1
            - name: test_service_2
        - services:
            - name: test_service_1
            - name: test_service_2

```

These test scenarios can then be executed with:
```
./generate-testcase.py -t templates/testconfiguration.tpl.yaml -f 2 -i | casual-xatmi-test-client --format-output json
time ./generate-testcase.py -t templates/testcase.tpl.yaml -i 1 -f 1 --type call-arbitary-service | casual-xatmi-test-client --format-output json
```

## `monitor-analyze.py`

Help:
```
usage: Create utilization report [-h] -r RESULTS -m METRIC [-f FILTER]

optional arguments:
  -h, --help            show this help message and exit
  -r RESULTS, --results RESULTS
                        utilization results in json
  -m METRIC, --metric METRIC
                        metric to show
  -f FILTER, --filter FILTER
                        filter on command
```

This script creates a summary of the result of `monitor.py`, which monitors utilization on the SUT.

`METRIC` can be one of `cpu`, `size`, `vsize` or `rss`.

`FILTER` is a free text that will be matched against the process command.

Example:
```
$ ./monitor-analyze.py -r results/testfall-1-250-100-other-20171129104638/domain1-utilization.json -m cpu -f casual
CPU
command                             min      max      avg
casual-xatmi-test-client.other      1.0      8.0     1.12
casual-xatmi-test-server.other      0.0      1.1     0.76
casual-xatmi-test-server.other      0.0      1.1     0.76
```

## `monitor.py`

Help:
```
usage: monitor.py [-h] --hosts HOSTS [HOSTS ...] [-i INTERVAL] [-d DURATION]
                  [--users USERS [USERS ...]] [--result-dir RESULT_DIR] -t
                  TESTCASE [-s SYSTEM]

Monitors CPU, memory etc.

optional arguments:
  -h, --help            show this help message and exit
  --hosts HOSTS [HOSTS ...]
                        Hosts that should be monitored
  -i INTERVAL, --interval INTERVAL
                        Interval (seconds) of collecting monitoring
                        information
  -d DURATION, --duration DURATION
                        Duration (seconds) that the hosts should be monitored
  --users USERS [USERS ...]
                        Usernames for the specified hosts for which the
                        domains are running as
  --result-dir RESULT_DIR
                        Base directory to store results
  -t TESTCASE, --testcase TESTCASE
                        Testcase that is executing
  -s SYSTEM, --system SYSTEM
                        Type of the system to be monitored
```

Simple script that will monitor utilization for the specified user on a SUT (with `ps`). The script is designed to be executed on a `staging`/`login` server that can reach all hosts via SSH.
It will login to the SUT with the same username (assuming public-key authentication) as the one logged into the `staging`-server, then use `sudo` to access the user which is executing the domain.

The script will collect utilization every `ITERVAL` for `DURATION` seconds.

Before starting collecting utilization, log files and statistics will be reset. After `DURATION` has passed, the log files and statistics will be gathered as well.

Example:
```
./monitor.py -i 1 -d 300 --hosts domain1 domain2 --users duser1 duser2 -t testcase-1 -s other
...
Results are stored in results/testcase-1-other-20171205110511/
```
