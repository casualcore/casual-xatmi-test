
# casual xatmi test

## objective

Provide some general _test suite_ to help test xatmi pattern


## parts

### casual-xatmi-test-server

A generic server that one configure at runtime _(with casual-xatmi-test-client)_

#### Semantics 
Advertise the `task` service to arbitary service names. Hence be able to set up 
logical paths from one instance to another, and from one domain to another. 




### casual-xatmi-test-resource-db2

A server that is linked to a `db2-resource`. It has the following service:

* xatmi/test/resouce/db2/delete
* xatmi/test/resouce/db2/insert
* xatmi/test/resouce/db2/select


### casual-xatmi-test-client

Reads tasks from `stdin`, performs the tasks, and print the result to `stdout`


## configuration



### task


We got an objective that consist of `0..*` tasks, which can have nested tasks, and so on...

```
objective:

  tasks:
    # 0..* tasks
```

Each task consist of the following attributes

name        | description
------------|------------------------
type        | the type of the task (string)
description | arbitary string to increase understanding
value       | the value for the type
payload     | not used 
tasks       | sub-tasks.



The following tasks are avalible 

name                        | value meaning | description
----------------------------|---------------|------------------------
call-service                | service-name  |  calls configured _task-service_ in  casual-xatmi-test-server
call-service-async          | service-name  | async-calls configured _task-service_ in  casual-xatmi-test-server
transaction-begin           | <no-op>       | start a transaction
transaction-commit          | <no-op>       | commit current transaction
transaction-rollback        | <no-op>       | roll back current transaction
call-arbitary-service       | service-name  | calls arbitary service (end point)
call-arbitary-service-async | service-name  | async-calls arbitary service (end point)
iterate                     | # iterations  | iterate over subtasks _value #_
sequential                  | <no-op>       | every subtask is done to completion before next subtask





#### example

```yaml
objective:

  tasks:
    - type: call-service-async
      value: s1
      tasks:
        - type: call-service-async
          value: s3
        - type: call-service-async
          value: s4
        - type: call-arbitary-service-async
          value: xatmi/test/resouce/db2/insert
          # no sub-tasks will be executed
               
    - type: call-service-async
      value: s2
      tasks:
        - type: sequential
          # sequential block (probably always good when fiddling with transaction)
          tasks:
           - type: transaction-begin
           
           - type: call-service-async
             value: s3
           - type: call-service-async
             value: s4
   
           - type: transaction-commit
```


### task services

Configuration to set up `casual-xatmi-test-server` to expose _task-services_ with specified names

#### example

The following will configure 4 instances to advertise `s1` and `s2`for the
first two, and `s3` and `s4` for the last two.

Note that you need to have configured at least as many instances for `casual-xatmi-test-server`
as there are entries in the setup-list.

```yaml

objective:
  setup:
    - services:
      - name: s1
      - name: s2
    - services:
      - name: s1
      - name: s2
    - services:
      - name: s3
      - name: s4
    - services:
      - name: s3
      - name: s4

```





